# Java Maven DevOps Project

Welcome to the AWS Jenkins-CI/CD branch of the Java Maven DevOps project!

## Requirements

To effectively contribute to this Java Maven DevOps project and work on the Jenkins-CI/CD branch, the following skills and knowledge are essential:

- **Software Development Life Cycle (SDLC):** A solid understanding of the Software Development Life Cycle is crucial for this project. Knowledge of various SDLC methodologies, such as Agile, Scrum, or Waterfall, will help in planning, executing, and managing the development process efficiently.

- **Groovy and Groovy Scripts:** Since the Jenkins pipeline uses Groovy scripts, a strong command of Groovy is necessary. Familiarity with Groovy syntax, data types, control structures, and functions will be essential for writing and customizing the pipeline stages and shared library code.

- **Declarative Jenkins Syntax:** Proficiency in writing Jenkins pipelines using the declarative syntax is vital. Declarative pipelines offer a more structured and concise approach to defining CI/CD workflows, making the pipeline code easy to read and maintain.

## Purpose

This branch focuses on setting up a Jenkins pipeline using a Jenkinsfile and Groovy script to demonstrate Continuous Integration and Continuous Deployment (CI/CD) with Jenkins. The pipeline will automate various stages, including version incrementing, application building, Docker image creation, and deployment using Docker Compose.

### Jenkins Pipeline Stages

**Stage 1: Increment Version**

In this stage, the Jenkins pipeline will automatically increment the patch version in the `pom.xml` file of the Java Maven project. This ensures that each build has a unique version number.

**Stage 2: Build App**

The "Build App" stage compiles the Java Maven application, runs unit tests to ensure code quality and correctness, and creates an executable JAR file.

**Stage 3: Build Docker Image**

In this stage, the Jenkins pipeline builds a Docker image of the Java Maven application using the Dockerfile and the updated version obtained from the previous stage. The Docker image is then tagged with the appropriate version for easy identification.

**Push to Docker Hub**

Once the Docker image is successfully built, the pipeline logs into Docker Hub using credentials provided as environment variables. It then pushes the newly created Docker image with the updated version to the designated repository on Docker Hub.

**Stage 4: Deploy using Docker Compose on AWS EC2 **

In this stage, the pipeline uses Docker Compose to deploy the Java Maven application on ec2 server.

### Shared Library

To enhance reusability and maintainability, this Jenkins pipeline utilizes a shared library. The shared library contains common Jenkins functions and custom steps that can be reused across different Jenkins pipelines. You can find the shared library repository at: [Jenkins Shared Library Repository](https://gitlab.com/zoro459/jenkins-shared-library)

## Getting Started

To get started with the Jenkins pipeline, ensure you have a Jenkins server set up with appropriate plugins, make sure to set up the necessary credentials in Jenkins to access the repository and Docker Hub and AWS.

## Webhook Configuration

### Pipeline (GitLab Webhook)

If you are using a regular pipeline for your Jenkins jobs and your code is hosted on GitLab, follow these steps to configure the GitLab webhook:

1. **Install the "GitLab Plugin" in Jenkins:** Go to Jenkins > Manage Jenkins > Manage Plugins > Available tab, search for "GitLab Plugin," and install it.

2. **Configure the GitLab API Token:** In Jenkins, go to Manage Jenkins > Configure System > GitLab section. Add your GitLab API Token to establish the connection between Jenkins and GitLab.

3. **Configure GitLab Integration:** In your GitLab repository, navigate to Settings > Integrations. Add the URL of your Jenkins server and the GitLab API Token configured in Jenkins. This allows GitLab to notify Jenkins of any new commits or changes.


## Contribution and Feedback

This project is a continuous learning journey, and I welcome any feedback, suggestions, or contributions from the community. If you have ideas to enhance the pipeline, propose new tools to integrate, or find any issues, feel free to open an issue or submit a pull request.

Let's build and improve together! Happy CI/CD!

---

Have a question? Need help? Reach out to [my-linkedin](https://www.linkedin.com/in/mohamed-osama-789249278).

