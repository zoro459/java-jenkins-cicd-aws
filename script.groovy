#!/usr/bin/env groovy
def Versioning(){
            echo 'incrementing app version...'
            sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit'
            def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
            def version = matcher[0][1]
            env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}
def DeployApp(){
            def cmd = "bash ./serverScript.sh ${env.IMAGE_NAME}"
            sshagent(['ec2-server-ssh']) {
            sh 'scp docker-compose.yaml ec2-user@52.23.210.52:/home/ec2-user'
            sh 'scp serverScript.sh ec2-user@52.23.210.52:/home/ec2-user'
            sh "ssh -o StrictHostKeyChecking=no ec2-user@52.23.210.52 $cmd"

}
}
return this
